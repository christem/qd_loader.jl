#using qd_loader
using qd_analysis
using LibPQ
using Test

include("../src/load_acq_event.jl")

function profile_conn()
    id = 17453
    conn = create_connection()
    max_num_captures = get_max_index(conn,id)
    print(max_num_captures)
    channels, event = load_acq_event(conn, id, sample_channel=6, load_range=0:3)
    close(conn)
end

#compilation
#@profview profile_conn()
#pure runtime
#@profview profile_conn()


id = 17459
#if !(@isdefined channels)
conn = create_connection()
max_num_captures = get_max_index(conn,id)
channels, event = load_acq_event(conn, id, load_range=0:2, sample_channel=6, custom_scaler=[(6,1.0)])
close(conn)

#plot_time_domain([channels[6][1:998:end]], time_scale=:rel)
unwrapped = unwrap_sawtooth(channels[6], 2^19)
# unwrapped_zeroed = (unwrapped - unwrapped[1])/400000

plot_time_domain([channels[1][1:1000:end]], time_scale=:rel)
