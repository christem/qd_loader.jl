using JSON3
using qd_analysis
using StatsBase
using StructArrays
using ProgressMeter
include("helper.jl")

# Structs to represent postmortem events and waveform data
struct postmortem_events
    id::Int32
    timestamp::Int64
    channels::Vector{String}
    num_captures::Int32
    pm::Bool
    event_metadata::JSON3.Object{Base.CodeUnits{UInt8, String}, Array{UInt64, 1}}
    waveform_metadata::Union{JSON3.Object{Base.CodeUnits{UInt8, String}, Array{UInt64, 1}}, Nothing} 
end

function get_max_index(conn, id::Integer;)
    max_id = execute_query(conn, "SELECT MAX(sequence_id) AS max_sequence_id FROM postmortem_buffers WHERE event_id = \$1", [id])

    return max_id[1][1]
end

function create_connection(;host_name=ENV["DB_HOST"], port=ENV["DB_PORT"], db_name=ENV["DB_NAME"], user=ENV["DB_USER"], pass=ENV["DB_PASSWORD"])
    connect_string = "host=$host_name port=$port dbname=$db_name user=$user password=$pass"

    return LibPQ.Connection(connect_string)
end


# Load DAC acquisition events from a database
function load_acq_event(conn, idx::Integer;load_range::UnitRange{Int}=1:1, sample_channel:: Union{Integer, Nothing}=nothing, data_scaler::Integer = 2^20, custom_scaler::Union{Nothing, Vector{<:Tuple{<:Integer, <:Number}}} = nothing, channels_to_load::Union{Vector{<:Integer}, Nothing}=nothing)

    # Form query
    select_event_querys = "SELECT * FROM postmortem_events WHERE id = " * string(idx)
    get_channels_query = "SELECT DISTINCT channel_id FROM postmortem_buffers WHERE event_id = " * string(idx) * " ORDER BY channel_id"
    get_num_captures_query = "SELECT COUNT(DISTINCT sequence_id) FROM postmortem_buffers WHERE event_id = " * string(idx)
    get_waveform_id_query = "SELECT waveform_id FROM postmortem_dac_waveform WHERE event_id = " * string(idx)
    get_waveform_query = "SELECT * FROM dac_waveforms WHERE id = " * string(execute_query(conn, get_waveform_id_query)[1][1])

    # Fetch events
    event_data = execute_query(conn, select_event_querys)
    channels = execute_query(conn, get_channels_query)
    num_captures = execute_query(conn, get_num_captures_query)
    waveform_data = execute_query(conn, get_waveform_query)

    
    # Put all these variables in temporary variables

    id_t = convert(Int32, event_data[1][1])
    timestamp_t = datetime2epoch(DateTime(convert(TimeZones.ZonedDateTime, event_data[2][1]), UTC))
    channels_t = convert(Vector{String}, channels[1])
    num_captures_t = (convert(Int32, num_captures[1][1]) == 0) ? 1 : convert(Int32, num_captures[1][1])
    pm_t = convert(String, event_data[4][1]) == "postmortem"
    event_metadata_t = JSON3.read(convert(String, event_data[3][1]))
    
    if ismissing(waveform_data[2][1])
        waveform_metadata_t = nothing
    else
        waveform_metadata_t = JSON3.read(convert(String, waveform_data[2][1]))
    end
    
    events = postmortem_events(id_t,timestamp_t,channels_t,num_captures_t,pm_t,event_metadata_t,waveform_metadata_t)

    # Check that load_range is valid for the number of captures
    if length(load_range) > events.num_captures
        throw(ArgumentError("load_range is larger than the number of captures"))
    end
    if first(load_range) > events.num_captures || last(load_range) > events.num_captures
        throw(ArgumentError("load_range is larger than the number of captures"))
    end

    # Query to get the number of datapoints per channel in the specified load_range
    lenght_matrix = zeros(Int32, length(events.channels), length(load_range))
    for (idx_j,i) in enumerate(load_range)
        for (idx_i, channel_name) in enumerate(events.channels)
            if events.pm
                get_capture_channel_length_query = "SELECT ARRAY_LENGTH(postmortem_data , 1) FROM postmortem_buffers WHERE event_id = " * string(idx) * " AND channel_id = '" * channel_name * "'"  # Legacy PM captures do not have sequence_id
            else
                get_capture_channel_length_query = "SELECT ARRAY_LENGTH(postmortem_data , 1) FROM postmortem_buffers WHERE event_id = " * string(idx) * " AND channel_id = '" * channel_name * "' AND sequence_id = " * string(i)
            end
            lenght_matrix[idx_i, idx_j] = execute_query(conn, get_capture_channel_length_query)[1][1]
        end
    end

    #Check that all values in the column are the same
    if any(lenght_matrix .!= lenght_matrix[:,1])
        throw(ArgumentError("The number of datapoints in the specified load_range is not the same for all channels"))
    end

    # Get the number of datapoints per channel and initialize a datamatrix
    if !isnothing(channels_to_load) && length(channels_to_load) < length(events.channels)
        lenght_matrix = lenght_matrix[channels_to_load, :]
        events = postmortem_events(id_t,timestamp_t,events.channels[channels_to_load],num_captures_t,pm_t,event_metadata_t,waveform_metadata_t)
    end

    datapoints = zeros(Int32,sum(lenght_matrix,dims=2)[1],length(events.channels))

        # Initialize progress bar for fetching data
    total_operations = length(load_range) * length(events.channels)
    p = Progress(total_operations, desc="Fetching Data", barlen=20)

    # Fetch the data from the desired load_range and the channels. Put them in the num_datapoints matrix
    for (idx_j,i) in enumerate(load_range)
        for (idx_i, channel_name) in enumerate(events.channels)
            data_length = lenght_matrix[idx_i, idx_j]
            previous_data_length = sum(lenght_matrix[idx_i,1:idx_j])
            range = previous_data_length-data_length+1:previous_data_length
            if events.pm
                get_data_query = "SELECT postmortem_data FROM postmortem_buffers WHERE event_id = " * string(idx) * " AND channel_id = '" * channel_name * "'"
            else
                get_data_query = "SELECT postmortem_data FROM postmortem_buffers WHERE event_id = " * string(idx) * " AND channel_id = '" * channel_name * "' AND sequence_id = " * string(i)
            end
            #datapoints[range, idx_i] = execute_query(conn, get_data_query)[1][1]
            datapoints[range, idx_i] = execute_fast_int_array_query(conn, get_data_query)
            next!(p)
        end
    end

    # Fetch sample frequency
    sample_freq = 40e6/events.event_metadata["regs"]["GlobalHeartBeatReg"]["raw_val"]

    total_columns = size(datapoints, 2)  # The number of columns corresponds to the number of captures

    captures = Vector{Capture}(undef, total_columns)  # Preallocate the captures array for better performance

    data_scaler_array = repeat([data_scaler], length(events.channels))

    # Change scalar if specified
    if(!isnothing(custom_scaler))
        for (idx, scaler) in custom_scaler
            if idx > length(data_scaler_array)
                throw(ArgumentError("The specified apply_scaler is larger than the number of channels"))
            end
            data_scaler_array[idx] = scaler
        end
    end

    if(isnothing(sample_channel))
        p_captures = Progress(total_columns, desc="Creating Captures", barlen=20)
        if events.waveform_metadata == nothing
            waveform = nothing
        else
            waveform = extract_waveform(events.waveform_metadata)
        end
        for (idx, col) in enumerate(eachcol(datapoints))
            if data_scaler_array[idx] == 1.0
                captures[idx] = Capture(Vector(col), sample_rate=sample_freq, time_start=events.timestamp/10^9, stimuli=waveform) # To avoid floating point issues
            else
                captures[idx] = Capture(col/data_scaler_array[idx], sample_rate=sample_freq, time_start=events.timestamp/10^9, stimuli=waveform)
            end
            next!(p_captures)  # Increment the progress bar
        end
    else
        p_captures = Progress(total_columns-1, desc="Creating Captures", barlen=20)
        if sample_channel > length(events.channels)
            throw(ArgumentError("The specified sample_channel is larger than the number of channels"))
        end
        # Extract Samples
        sample_point = datapoints[:, sample_channel]
        sample_points_unwrapped = unwrap_sawtooth(sample_point, 2^(19))
        gaps = find_large_gaps(sample_points_unwrapped)
        print("GAPS:")
        print(gaps)
        # Calculate streaming sample_rate
        decimation = mode(diff(sample_point))
        print(decimation)
        sample_freq = sample_freq/decimation
        if events.waveform_metadata == nothing
            waveform = nothing
        else
            waveform = extract_waveform(events.waveform_metadata, decimation=decimation)
        end
        for (idx, col) in enumerate(eachcol(datapoints))
            if idx == sample_channel
                if data_scaler_array[idx] == 1.0
                    captures[idx] = Capture(Vector(sample_point), sample_rate=sample_freq, time_start=events.timestamp/10^9, stimuli=nothing) # To avoid floating point issues
                else
                    captures[idx] = Capture(sample_point/data_scaler_array[idx], sample_rate=sample_freq, time_start=events.timestamp/10^9, stimuli=nothing)
                end
            else
                tmp_capture = Capture(col/data_scaler_array[idx], sample_rate=sample_freq, time_start=events.timestamp/10^9, stimuli=waveform)
                capture_segments = Vector{Capture}(undef, length(gaps))
                for (idx, gap) in enumerate(gaps)
                    tmp_capture_segment = nonuniform_to_uniform_sampling(tmp_capture[gap[1]], sample_points_unwrapped[gap[1]])
                    vals = [tmp_capture_segment.val; repeat([NaN], Integer(round(gap[2]/decimation)))]
                    capture_segments[idx] = Capture(vals, sample_freq, events.timestamp/10^9, waveform)
                end
                #Capture_segments = vcat(capture_segments...)
                tmp = vcat(capture_segments...)
                captures[idx] = tmp[1:end] # To force recalculation of gap free indexes. THIS IS A HACK
            end
            next!(p_captures)  # Increment the progress bar
        end
    end

    return captures, events
end