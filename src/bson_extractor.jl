using JSON3
using StructArrays
using LightBSON
using TensorCast
using qd_analysis



function extract_sine(json_sine)
    amplitude = json_sine["amplitude"]
    frequency = json_sine["frequency"]
    phase = json_sine["phase"]

    sine = Sine(frequency, phase, amplitude)

    return sine
end

function extract_waveform(json_waveform)
    
    if  haskey(json_waveform, "Multisine")
        sines = StructArray(extract_sine.(json_waveform["Multisine"]["Sines"]))
        waveform = Multisine(sines, json_waveform["Multisine"]["total_length"])
    elseif haskey(json_waveform, "PWM")
        waveform = PWM(json_waveform["PWM"]["period"], json_waveform["PWM"]["duty_cycle"], json_waveform["PWM"]["amplitude"], 0)
    elseif haskey(json_waveform, "Impulse")
        waveform = Impulse(json_waveform["Impulse"]["period"], json_waveform["Impulse"]["amplitude"], 0)
    end

    return waveform
end


function extract_channel_config(char_json, sample_rate, digital_gain)

    temp_trans = char_json["transfer_function"]
    @cast transfer_function[i,j] |= temp_trans[i][j]
    transfer_function = vec(transfer_function[:]).*1im
    return ChannelConfig(sample_rate,
        char_json["full_scale"],
        char_json["gain_settings"][1],
        digital_gain,
        char_json["gain_corrections"][1],
        transfer_function,
        char_json["bit_depth"])
end

"Copy a JSON3.Array containing real valued elements into a regular Vector{Float64}."
function typed_copy(j3arr)
    n = length(j3arr)
    x = Vector{Int64}(undef, n)
    for i in 1:n
        x[i] = j3arr[i]
    end
    return x
end


function to_mat(arrs)
    return Matrix(hcat(typed_copy.(arrs)...))
end


function to_rep_sweep_acq_mat(arrs)
    rows = length(arrs)
    cols = length(arrs[1])
    mat = Matrix{Matrix{Int64}}(undef, rows, cols)
    for i in 1:rows
        for j in 1:cols
            #mat[i,j] = to_mat(arrs[i][j])
            mat[i,j] = reduce(hcat,arrs[i][j])
        end
    end
    return mat
end

function to_rep_sweep_time_vector(arrs)
    rows = length(arrs)
    cols = length(arrs[1])
    mat = Matrix{Vector{Int64}}(undef, rows, cols)
    for i in 1:rows
        for j in 1:cols
            mat[i,j] = vec(to_mat(arrs[i][j]))
            #mat[i,j] = reduce(hcat,arrs[i][j])
        end
    end
    return mat
end

function extract_time(json)
    return convert(Vector{Int64},json["adc"]["time"])
end

function extract_written_waveform(json)

    len = length(json["dac"]["written_waveform"])
    written_waveform = Vector{Vector{Int64}}(undef, len)
    for (idx,i) in enumerate(json["dac"]["written_waveform"])
        written_waveform[idx] = convert(Vector{Int64},i)
    end
    return written_waveform[1]
end


function bool_to_neg(x::Bool)
    if x
        return 1
    else
        return -1
    end 
end
function extract_polarity_vector(json)
    neg_pol = convert(Vector{Bool},json["adc"]["neg_polarity"])
    return bool_to_neg.(neg_pol)
end

function extract_adc_configs(json)
    sample_rate = 40e6/json["agent_regs"]["GlobalHeartBeatReg"]["raw_val"]
    adc_channels = StructVector(extract_channel_config.(json["adc"]["channels"], sample_rate, 2.0)) #digital gain is not used for ADC channels
    #acquisition_matrix = to_rep_sweep_acq_mat(json["adc"]["acquisitions"])
    acq = json["adc"]["acquisitions"]
    @cast acquisition_matrix[i,j] |= acq[1][1][j][i]
    polarity = extract_polarity_vector(json)
    time = json["adc"]["time"]
    #time = to_rep_sweep_time_vector(json["adc"]["time"])
    time = @cast time[i] |= time[1][1][i]
    adc = AdcConfig_conv(adc_channels, polarity, acquisition_matrix, time)
    return adc
end

function extract_adc_configs(json, custom_channel_config)
    sample_rate = 40e6/json["agent_regs"]["GlobalHeartBeatReg"]["raw_val"]
    adc_channels = custom_channel_config
    #acquisition_matrix = to_rep_sweep_acq_mat(json["adc"]["acquisitions"])
    acq = json["adc"]["acquisitions"]
    @cast acquisition_matrix[i,j] |= acq[1][1][j][i]
    polarity = extract_polarity_vector(json)
    time = json["adc"]["time"]
    #time = to_rep_sweep_time_vector(json["adc"]["time"])
    time = @cast time[i] |= time[1][1][i]
    adc = AdcConfig_conv(adc_channels, polarity, acquisition_matrix, time)
    return adc
end

function extract_dac_config(json)

    sample_rate = 40e6/json["agent_regs"]["GlobalHeartBeatReg"]["raw_val"]
    trigger_idx = copy(transpose(to_mat(json["dac"]["trigger_idx"])))
    trigger_idx = trigger_idx[1]

    waveform = extract_waveform(json["dac"]["waveform"])
    dac_channel = extract_channel_config(json["dac"]["characterisation"], sample_rate, json["dac"]["digital_gain"]/2.0)


    written_waveform = extract_written_waveform(json)
    dac = DacConfig(dac_channel, waveform, trigger_idx, written_waveform)

    return dac
end

function bson_extract_data(path, channel_config_path::String = "")
    bson = bson_read(path)

    if(channel_config_path != "")
        sample_rate = 40e6/bson["agent_regs"]["GlobalHeartBeatReg"]["raw_val"]
        jsonString = open(f->read(f, String), channel_config_path)
        json_channel_config = JSON3.read(jsonString)
        channel_config = StructVector(extract_channel_config.(json_channel_config["channels"], sample_rate, 2.0)) #digital gain is not used for ADC channels
        adcs = extract_adc_configs(bson, channel_config)
    else
        adcs = extract_adc_configs(bson)
    end

    if haskey(bson, "dac")
        dac = extract_dac_config(bson)
        return AcqAdcDac(adcs,dac)
    end

    return AcqAdc(adcs)
    
end
