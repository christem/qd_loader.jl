using JSON3
#using NativeFileDialog
using FileIO
using JLD2
using qd_analysis

include("bson_extractor.jl")
#C:\Users\magnu\cernbox\CERN\phd\testData\D2_02_2023_matrix_test\measurements\measurement_magnet_warm_no_powerconverter\1_Emag_Mmag_C1V_C2I_C3I_2023-04-04_15-23-46.bson
# function load_file_picker(conf::String)

#     jsonString = open(f->read(f, String), conf)

#     json = JSON3.read(jsonString)

#     file_path = json.acquisition_save_path
#     file = pick_file(file_path)
#     println(file)

#     acquisition = load_bson_direct(file)
#     return acquisition
# end

function load_bson_direct(file_path::String)
    acquisition = bson_extract_data(file_path)
    return acquisition
end

function load_bson_direct(file_raw::DenseVector{UInt8})
    acquisition = bson_extract_data(file_raw)
    return acquisition
end

function load_bson_direct(file_path::String, channel_config_path::String)
    acquisition = bson_extract_data(file_path, channel_config_path)
    return acquisition
end

function bson_to_jld2(file_path::String)
    acquisition = bson_extract_data(file_path)
    acquisition = acquisition_to_capture(acquisition)
    file_name = split(file_path, ".")[1]*".jld2"
    jldsave(file_name; acquisition)
end

function load_Aqr_matrix(A_qr_path::String)
    A_qr = FileIO.load(A_qr_path,"A_qr")

    return A_qr
end