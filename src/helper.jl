using JSON3
using qd_analysis
using LibPQ
using Tables
using Dates
using TimeZones
using StructArrays




# Execute an SQL query and return results as a column table
function execute_query(conn, query, args=[])
    columntable(execute(conn, query, args))
end

function execute_fast_int_array_query(conn, query, args=[])
    r = execute(conn, query, args, binary_format=true)
    val = LibPQ.PQValue(r,1,1)
    p = LibPQ.bytes_view(val)
    return sql_arr_to_int_arr(p[21:end-1])
end

# Function to convert a DateTime to an epoch
datetime2epoch(x::DateTime) = (Dates.value(x) - Dates.UNIXEPOCH) * 1_000_000

# Extract sine wave properties from JSON
function extract_sine(json_sine; decimation::Integer=1)
    Sine(json_sine["frequency"]*decimation, json_sine["phase"], json_sine["amplitude"])
end

# Extract waveform properties from JSON
function extract_waveform(json_waveform; decimation::Integer=1)
    if haskey(json_waveform, "Multisine")
        sines = StructArray(extract_sine.(json_waveform["Multisine"]["Sines"], decimation=decimation))
        return Multisine(sines, Integer(round(json_waveform["Multisine"]["total_length"]/decimation)))      # We will have an issue if the total length is not exactly divisible by the decimation
    elseif haskey(json_waveform, "PWM")
        return PWM(json_waveform["PWM"]["period"], json_waveform["PWM"]["duty_cycle"], json_waveform["PWM"]["amplitude"], 0)
    elseif haskey(json_waveform, "Impulse")
        return Impulse(json_waveform["Impulse"]["period"], json_waveform["Impulse"]["amplitude"], 0)
    else
        return nothing
    end
end

function sql_arr_to_int_arr(vector::Vector{UInt8})
    # Length of the pattern to remove
    pattern_length = 4

    interval = 8
    # The number of intervals, assuming the vector's length is perfectly divisible
    num_intervals = length(vector) ÷ interval

    # Reshape the vector into a 2D array where each column represents an interval
    reshaped = reshape(vector, interval, num_intervals)

    # Select the parts of each interval to keep, excluding the pattern
    result = reshape(reshaped[(pattern_length + 1):end, :], :)

    return ntoh.(Vector{Int32}(reinterpret(Int32, result)))
end

