using JSON3
using LibPQ
using Tables
using Dates
using qd_analysis
using LightBSON
using TimeZones

struct postmortem_dac_waveform
    id::Int32
    event_id::Int32
    waveform_id::Int32
    uploaded_waveform::Vector{Int32}
    waveform_metadata::JSON3.Object{Base.CodeUnits{UInt8, String}, Array{UInt64, 1}}
    channel_id::String
end

# Function to convert a DateTime to an epoch
datetime2epoch(x::DateTime) = (Dates.value(x) - Dates.UNIXEPOCH) * 1_000_000

# Extract sine wave properties from JSON
function extract_sine(json_sine)
    Sine(json_sine["frequency"], json_sine["phase"], json_sine["amplitude"])
end

# Extract waveform properties from JSON
function extract_waveform(json_waveform)
    if haskey(json_waveform, "Multisine")
        sines = StructArray(extract_sine.(json_waveform["Multisine"]["Sines"]))
        return Multisine(sines, json_waveform["Multisine"]["total_length"])
    elseif haskey(json_waveform, "PWM")
        return PWM(json_waveform["PWM"]["period"], json_waveform["PWM"]["duty_cycle"], json_waveform["PWM"]["amplitude"], 0)
    elseif haskey(json_waveform, "Impulse")
        return Impulse(json_waveform["Impulse"]["period"], json_waveform["Impulse"]["amplitude"], 0)
    end
end

# Create a connection string for a database
function create_connection_string(host_name, port, db_name, user, pass)
    "host=$host_name port=$port dbname=$db_name user=$user password=$pass"
end

# Execute an SQL query and return results as a column table
function execute_query(conn, query, args=[])
    columntable(execute(conn, query, args))
end

function get_max_index(host_name::String, port::String, db_name::String, user::String, pass::String, id::Integer;)
    conn = LibPQ.Connection(create_connection_string(host_name, port, db_name, user, pass))

    max_id = execute_query(conn, "SELECT MAX(sequence_id) AS max_sequence_id FROM postmortem_buffers WHERE event_id = \$1", [id])

    return max_id[1][1]
end

# Load DAC acquisition events from a database
function load_dac_acq_event(host_name::String, port::String, db_name::String, user::String, pass::String, select_string::String; index::Integer=0)
    println("LOADING")
    # Establish a connection to the database
    conn = LibPQ.Connection(create_connection_string(host_name, port, db_name, user, pass))

    # Fetch events
    data = execute_query(conn, select_string)    
    events = postmortem_events.(
        convert(Vector{Int32}, data[1]),
        datetime2epoch.(DateTime.(convert(Vector{TimeZones.ZonedDateTime}, data[2]), UTC)),
        JSON3.read.(convert(Vector{String}, data[3]))
    )

    # Fetch waveform data and metadata for each event
    captures = Vector{Vector{Capture}}(undef, length(events))
    calibrations = Vector{Vector{FrequencyResponse}}(undef, length(events))
    for (id, event) in enumerate(events)
        waveform_data = execute_query(conn, "SELECT * FROM postmortem_dac_waveform WHERE event_id = \$1", [event.id])
        dac_waveform = postmortem_dac_waveform(
            convert(Int32, waveform_data[1][1]),
            event.id,
            convert(Int32, waveform_data[3][1]),
            convert(Vector{Int32}, waveform_data[4][1]),
            JSON3.read(convert(String, waveform_data[5][1])),
            convert(String, waveform_data[6][1])
        )
        waveform_json_data = execute_query(conn, "SELECT * FROM dac_waveforms WHERE id = \$1", [dac_waveform.waveform_id])
        waveform = extract_waveform(JSON3.read(convert(String, waveform_json_data[2][1])))
        #waveform_triggered = Dac_waveform_trigger(waveform, dac_waveform.waveform_metadata["dac_trigger_idx"])

        check_legacy =  execute_query(conn, "SELECT EXISTS (SELECT 1 FROM postmortem_buffers WHERE event_id = \$1 AND sequence_id = \$2) AS row_exists", [event.id, 0])
        if check_legacy[1][1]
            buffer_data = execute_query(conn, "SELECT postmortem_data, channel_id  FROM postmortem_buffers WHERE event_id = \$1 AND sequence_id = \$2", [event.id, index])
        else
            buffer_data = execute_query(conn, "SELECT postmortem_data, channel_id  FROM postmortem_buffers WHERE event_id = \$1", [event.id])
        end
        channels = convert(Vector{Vector{Int32}}, buffer_data[1]) ./ 2^20
        channel_ids = convert(Vector{String}, buffer_data[2])
        println("CHANNEL IDS")
        println(channel_ids)
        println("------------")
        # Construct the query with IN clause
        query = 
        """
        SELECT * FROM phd_io_cards_freq_response
        WHERE channel_id IN ($(join(["'$id'" for id in channel_ids], ", ")))
        """
        channel_data = execute_query(conn, query)
        calibrations[id] = FrequencyResponse.(convert.(Vector{Float64}, channel_data[2]), convert.(Vector{Float64}, channel_data[3]) + convert.(Vector{Float64}, channel_data[4])*im)
        channel_gains = convert(Vector{Float64}, channel_data[5])

        samples = collect(1:length(channels[1]))
        sample_freq = 40e6/event.event_metadata["regs"]["GlobalHeartBeatReg"]["raw_val"]
        sample_period = 1/sample_freq
        rel_time = samples .* sample_period
        abs_time = event.timestamp .+ (rel_time .* 1e9)

        captures[id] = Capture.(
            TimeDomain.(Ref(samples), Ref(rel_time), Ref(abs_time), channels, channel_gains, Ref(sample_freq)),
            Ref(Excitation(waveform, waveform, 0.00333)) # Where does this number come from?
        )
    end

    close(conn)
    return captures, calibrations, events
end



function db_upload_bson(acq_string::String, path::String, description::String)
    name = split(path,'\\')[end]
    split_path = split(split(name,'.')[1],'_')
    bson = bson_read(path)
    
    
    
    date = split_path[end-1]
    time = replace(split_path[end], '-' => ':')
    dateTime = date * " " * time * "+2"
    
    acq_metadata = Dict("regs" => bson["agent_regs"], "dac" => true, "name" => join(split_path[1:end-2], '_'), "description" => description)
    acq_metadata_json = JSON3.write(acq_metadata)
    
    dac_conf = Dict("dac_digital_gain" => bson["dac"]["digital_gain"], "dac_trigger_idx" => bson["dac"]["trigger_idx"][1][1])
    dac_conf_json = JSON3.write(dac_conf)
    dac_written_waveform = bson["dac"]["written_waveform"][1]
    dac_waveform = JSON3.write(bson["dac"]["waveform"])
    
    channel_num = length(bson["adc"]["acquisitions"][1][1])
    channel_text_arr = ["channel_" * string(i) for i in 1:channel_num]
    
    channels = [bson["adc"]["acquisitions"][1][1][i] for i in 1:channel_num]
    
    
    
    
    
    conn = LibPQ.Connection(acq_string)
    
    result = execute(conn, "SELECT EXISTS(SELECT 1 FROM dac_waveforms WHERE waveform = \$1);", [dac_waveform])
    data = columntable(result)
    if data[1][1] == false
        result = execute(conn, "INSERT INTO dac_waveforms (waveform) VALUES (\$1) RETURNING id;", [dac_waveform])
        data = columntable(result)
        wave_id = data[1][1]
    else
        result = execute(conn, "SELECT id FROM dac_waveforms WHERE waveform = \$1;", [dac_waveform])
        data = columntable(result)
        wave_id = data[1][1]
    end
    
    result = execute(conn, "INSERT INTO postmortem_events (event_time, event_metadata) VALUES (\$1, \$2) RETURNING id;", [dateTime, acq_metadata_json])
    data = columntable(result)
    id = data[1][1]
    
    LibPQ.load!(
        (event_id = [id], waveform_id = [wave_id], uploaded_waveform = [dac_written_waveform], metadata = [dac_conf_json], channel_id = ["dac_channel_1"]),
        conn,
        "INSERT INTO postmortem_dac_waveform (event_id, waveform_id, uploaded_waveform, metadata, channel_id) VALUES (\$1, \$2, \$3, \$4, \$5);",
    )
    
    LibPQ.load!(
        (event_id = [id, id, id], channel_id = channel_text_arr, postmortem_data=channels),
        conn,
        "INSERT INTO postmortem_buffers (event_id, channel_id, postmortem_data) VALUES (\$1, \$2, \$3);",
    )
    
    
    
    close(conn)
end

function db_upload_frq_response(acq_string::String, name::String, frequency::Vector{<:Real}, real::Vector{<:Real}, imaginary::Vector{<:Real})

    conn = LibPQ.Connection(acq_string)
    
    result = execute(conn, "INSERT INTO phd_io_cards_freq_response (channel_id, frequency, real, imaginary) VALUES (\$1, \$2, \$3, \$4) ;", [name, frequency, real, imaginary])

    close(conn)
end

function db_upload_channel_fft(acq_string::String, event_id, channel_id, frequency::Real, magnitude::Vector{<:Real})

    conn = LibPQ.Connection(acq_string)
    
    result = execute(conn, "INSERT INTO phd_spectrum_post_mortem_buffers (event_id, channel_id, fft_magnitude, fft_frequency_resolution) VALUES (\$1, \$2, \$3, \$4) ;", [event_id, channel_id, magnitude, frequency])

    close(conn)
end
