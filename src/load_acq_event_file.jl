using JSON3
using qd_analysis
using StatsBase
using StructArrays
using ProgressMeter
using Pickle  # For loading pickle files
using Glob
include("helper.jl")

# Structs to represent postmortem events and waveform data
struct postmortem_events
    id::Int32
    timestamp::Int64
    channels::Vector{String}
    num_captures::Int32
    pm::Bool
    event_metadata::JSON3.Object{Base.CodeUnits{UInt8, String}, Array{UInt64, 1}}
    waveform_metadata::Union{JSON3.Object{Base.CodeUnits{UInt8, String}, Array{UInt64, 1}}, Nothing} 
end

function get_event_data(base_path::String, event_id::Integer)
    event_dir = joinpath(base_path, "events")
    file_pattern = "$(event_id)_*.json"
    event_files = glob(file_pattern, event_dir)
    if length(event_files) == 0
        error("No event file found for event ID $event_id")
    end
    event_data = JSON3.read(open(event_files[1]) |> read)
    return event_data
end

function get_waveform_data(base_path::String, event_id::Integer)
    waveform_dir = joinpath(base_path, "dac_waveform")
    file_pattern = "dac_waveform_$(event_id)_*.json"
    waveform_files = glob(file_pattern, waveform_dir)
    if length(waveform_files) == 0
        return nothing
    end
    waveform_data = JSON3.read(open(waveform_files[1]) |> read)
    return waveform_data
end

function get_waveform(base_path::String, waveform_id::Integer)
    waveform_dir = joinpath(base_path, "waveforms")
    file_pattern = "$(waveform_id)_*.json"
    waveform_files = glob(file_pattern, waveform_dir)
    if length(waveform_files) == 0
        error("No waveform file found for waveform ID $waveform_id")
    end
    waveform_string = read(waveform_files[1], String)
    waveform = JSON3.read(replace(waveform_string, "\n" => ""))
    return waveform
end

function get_channels_pm_path(base_path::String, event_id::Integer)
    dir = joinpath(base_path, "pm")
    pm_dir = glob("$(event_id)_*", dir)
    channels = readdir(pm_dir[1])
    return channels
end

function get_channels_stream_num_channels(base_path::String, event_id::Integer)
    dir = joinpath(base_path, "stream")
    stream_dir = glob("$(event_id)_*", dir)
    channels = readdir(stream_dir[1])
    return length(channels)
end

function get_stream_size(base_path::String, event_id::Integer)
    dir = joinpath(base_path, "stream")
    stream_dir = glob("$(event_id)_*", dir)
    streams = glob("*.pkl", stream_dir[1])
    dat = Pickle.npyload(open(streams[2]))
    return length(streams), size(dat)[1], size(dat)[2]
end

function get_stream_data(base_path::String, event_id::Integer, file_idx::Integer, channel_idx::Integer)
    dir = joinpath(base_path, "stream")
    stream_dir = glob("$(event_id)_*", dir)
    streams = glob("*_$(file_idx).pkl", stream_dir[1])
    dat = Pickle.npyload(open(streams[1]))
    return dat[:, channel_idx]
end

function get_buffer_data(base_path::String, event_id::Integer, channel_id::String)
    buffer_dir = joinpath(base_path, "pm")
    dir_pattern = "$(event_id)_*"
    buffer_dir = glob(dir_pattern, buffer_dir)
    buffer_files = glob(channel_id, buffer_dir[1])
    if length(buffer_files) == 0
        error("No buffer file found for event ID $event_id and channel ID $channel_id")
    end
    buffer_data = Vector{Int32}(Pickle.load(open(buffer_files[1]))["postmortem_data"])
    return buffer_data
end

function load_acq_event_file(base_path::String, event_id::Integer; load_range::UnitRange{Int}=1:1, sample_channel::Union{Integer, Nothing}=nothing, data_scaler::Integer = 2^20, custom_scaler::Union{Nothing, Vector{<:Tuple{<:Integer, <:Number}}} = nothing)
    # Load event data
    event_data = get_event_data(base_path, event_id)
    waveform_data = get_waveform_data(base_path, event_id)
    waveform_id = waveform_data["waveform_id"]
    waveform_metadata_t = waveform_id == nothing ? nothing : get_waveform(base_path, waveform_id)
    
    id_t = convert(Int32, event_data["id"])

    format = DateFormat("yyyy-mm-dd_HH-MM-SS")
    timestamp_t = datetime2epoch(DateTime(event_data["event_time"], format))
    pm_t = event_data["acquisition_type"] == "postmortem"

    file_samples = 0
    if pm_t
        channels_t = get_channels_pm_path(base_path, event_id)
        num_captures_t = 1
    else
        num_captures_t, file_samples, num_channels = get_stream_size(base_path, event_id)

        channels_t = string.(collect(1:num_channels))
    end

    event_metadata_t = JSON3.read(replace(event_data["event_metadata"], "\n" => ""))
    
    events = postmortem_events(id_t, timestamp_t, channels_t, num_captures_t, pm_t, event_metadata_t, waveform_metadata_t)

    # Check that load_range is valid for the number of captures
    if length(load_range) > events.num_captures
        throw(ArgumentError("load_range is larger than the number of captures"))
    end
    if first(load_range) > events.num_captures || last(load_range) > events.num_captures
        throw(ArgumentError("load_range is larger than the number of captures"))
    end

    # Initialize length matrix
    if pm_t
        lenght_matrix = zeros(Int32, length(events.channels), length(load_range))
        for (idx_j, i) in enumerate(load_range)
            for (idx_i, channel_name) in enumerate(events.channels)
                buffer_data = get_buffer_data(base_path, event_id, channel_name)
                lenght_matrix[idx_i, idx_j] = length(buffer_data)
            end
        end
    else
        lenght_matrix = fill(file_samples, length(events.channels), num_captures_t)
    end

    # Check that all values in the column are the same
    if any(lenght_matrix .!= lenght_matrix[:, 1])
        throw(ArgumentError("The number of datapoints in the specified load_range is not the same for all channels"))
    end

    # Get the number of datapoints per channel and initialize a data matrix
    datapoints = zeros(Int32, sum(lenght_matrix, dims=2)[1], length(events.channels))

    # Initialize progress bar for fetching data
    total_operations = length(load_range) * length(events.channels)
    p = Progress(total_operations, desc="Fetching Data", barlen=20)

    # Fetch the data from the desired load_range and the channels
    if pm_t 
        for (idx_j, i) in enumerate(load_range)
            for (idx_i, channel_name) in enumerate(events.channels)
                buffer_data = get_buffer_data(base_path, event_id, channel_name)
                data_length = lenght_matrix[idx_i, idx_j]
                previous_data_length = sum(lenght_matrix[idx_i, 1:idx_j])
                range = previous_data_length-data_length+1:previous_data_length
                datapoints[range, idx_i] = buffer_data
                next!(p)
            end
        end
    else
        if load_range == 1:1
            load_range = 1:num_captures_t
        end
        for (idx_j, i) in enumerate(load_range)
            for (idx_i, channel_name) in enumerate(events.channels)
                buffer_data = get_stream_data(base_path, event_id, idx_j-1, idx_i)
                data_length = lenght_matrix[idx_i, idx_j]
                previous_data_length = sum(lenght_matrix[idx_i, 1:idx_j])
                range = previous_data_length-data_length+1:previous_data_length
                datapoints[range, idx_i] = buffer_data
                next!(p)
            end
        end
    end


    # Fetch sample frequency
    sample_freq = 40e6 / events.event_metadata["regs"]["GlobalHeartBeatReg"]["raw_val"]

    total_columns = size(datapoints, 2)  # The number of columns corresponds to the number of captures
    captures = Vector{Capture}(undef, total_columns)  # Preallocate the captures array for better performance
    data_scaler_array = repeat([data_scaler], length(events.channels))

    # Change scalar if specified
    if !isnothing(custom_scaler)
        for (idx, scaler) in custom_scaler
            if idx > length(data_scaler_array)
                throw(ArgumentError("The specified apply_scaler is larger than the number of channels"))
            end
            data_scaler_array[idx] = scaler
        end
    end

    if isnothing(sample_channel)
        p_captures = Progress(total_columns, desc="Creating Captures", barlen=20)
        waveform = events.waveform_metadata == nothing ? nothing : extract_waveform(events.waveform_metadata)
        for (idx, col) in enumerate(eachcol(datapoints))
            if data_scaler_array[idx] == 1.0
                captures[idx] = Capture(Vector(col), sample_rate=sample_freq, time_start=events.timestamp / 10^9, stimuli=waveform)
            else
                captures[idx] = Capture(col / data_scaler_array[idx], sample_rate=sample_freq, time_start=events.timestamp / 10^9, stimuli=waveform)
            end
            next!(p_captures)  # Increment the progress bar
        end
    else
        p_captures = Progress(total_columns - 1, desc="Creating Captures", barlen=20)
        if sample_channel > length(events.channels)
            throw(ArgumentError("The specified sample_channel is larger than the number of channels"))
        end
        sample_point = datapoints[:, sample_channel]
        sample_points_unwrapped = unwrap_sawtooth(sample_point, 2^(19))
        gaps = find_large_gaps(sample_points_unwrapped)
        decimation = mode(diff(sample_point))
        sample_freq = sample_freq / decimation
        waveform = events.waveform_metadata == nothing ? nothing : extract_waveform(events.waveform_metadata, decimation=decimation)
        for (idx, col) in enumerate(eachcol(datapoints))
            if idx == sample_channel
                if data_scaler_array[idx] == 1.0
                    captures[idx] = Capture(Vector(sample_point), sample_rate=sample_freq, time_start=events.timestamp / 10^9, stimuli=nothing)
                else
                    captures[idx] = Capture(sample_point / data_scaler_array[idx], sample_rate=sample_freq, time_start=events.timestamp / 10^9, stimuli=nothing)
                end
            else
                tmp_capture = Capture(col / data_scaler_array[idx], sample_rate=sample_freq, time_start=events.timestamp / 10^9, stimuli=waveform)
                capture_segments = Vector{Capture}(undef, length(gaps))
                for (idx, gap) in enumerate(gaps)
                    tmp_capture_segment = nonuniform_to_uniform_sampling(tmp_capture[gap[1]], sample_points_unwrapped[gap[1]])
                    vals = [tmp_capture_segment.val; repeat([NaN], Integer(round(gap[2] / decimation)))]
                    capture_segments[idx] = Capture(vals, sample_freq, events.timestamp / 10^9, waveform)
                end
                captures[idx] = vcat(capture_segments...)
            end
            next!(p_captures)  # Increment the progress bar
        end
    end

    return captures, events
end
