using Revise
using qd_analysis
using qd_loader 
using LibPQ
using Tables
using JSON3
using TimeZones
using Dates
using StructArrays

select = "SELECT * FROM postmortem_events WHERE id = 17345"

id = 17345

max_idx = get_max_index(ENV["DB_HOST"], ENV["DB_PORT"], ENV["DB_NAME"], ENV["DB_USER"], ENV["DB_PASSWORD"], id)
acquisition, calibrations, event = load_dac_acq_event(ENV["DB_HOST"], ENV["DB_PORT"], ENV["DB_NAME"], ENV["DB_USER"], ENV["DB_PASSWORD"], select)

print("This is it!")