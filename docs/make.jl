using qd_loader
using Documenter

DocMeta.setdocmeta!(qd_loader, :DocTestSetup, :(using qd_loader); recursive=true)

makedocs(;
    modules=[qd_loader],
    authors="Magnus Christensen <magnus.christensen@cern.ch> and contributors",
    repo="https://github.com/christem/qd_loader.jl/blob/{commit}{path}#{line}",
    sitename="qd_loader.jl",
    format=Documenter.HTML(;
        prettyurls=get(ENV, "CI", "false") == "true",
        edit_link="main",
        assets=String[],
    ),
    pages=[
        "Home" => "index.md",
    ],
)
