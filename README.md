# qd_loader

# Database Schema Documentation

## Overview

This document outlines the structure and relationships of tables within the Postmortems database, focusing on the handling and analysis of postmortem data. The schema includes tables for storing waveform data, postmortem events, DAC waveforms associated with those events, and buffer data related to each event.

## Tables and Relationships

### 1. `dac_waveforms`

This table stores digital-to-analog converter (DAC) waveforms.

- **Columns:**
  - `id`: Unique identifier for each waveform. (Primary Key)
  - `waveform`: Waveform data stored in a JSONB format.

- **Indexes:**
  - Primary Key on `id`
  - Index on `id` (`dac_waveforms_id_idx`)

- **Relationships:**
  - Referenced by `io_qualification_raw_data` on `waveform_id`.
  - Associated with `postmortem_dac_waveform` through the `waveform_id` foreign key.

### 2. `postmortem_events`

Stores events related to postmortem analysis.

- **Columns:**
  - `id`: Unique identifier for each event. (Primary Key)
  - `event_time`: Timestamp with timezone for when the event occurred.
  - `event_metadata`: JSONB field for storing metadata about the event.

- **Indexes:**
  - Primary Key on `id`
  - GIN index on `event_metadata` (`postmortem_events_event_metadata_idx`)
  - BTree index on `event_time` (`postmortem_events_event_time_idx`)

- **Relationships:**
  - Referenced by:
    - `postmortem_buffers` on `event_id`.
    - `postmortem_dac_waveform` on `event_id`.
    - `phd_postmortem_events_impedance` on `event_id`.
    - `work_queue` on `id`.

### 3. `postmortem_dac_waveform`

Links postmortem events to DAC waveforms.

- **Columns:**
  - `id`: Unique identifier. (Primary Key)
  - `event_id`: Reference to an event in `postmortem_events`.
  - `waveform_id`: Reference to waveform data in `dac_waveforms`.
  - `uploaded_waveform`: Array of integers representing the uploaded waveform.
  - `metadata`: JSONB field for additional metadata.
  - `channel_id`: Text field for identifying the channel.

- **Indexes:**
  - Primary Key on `id`
  - Unique constraint on `event_id`
  - Multiple BTree indexes on `channel_id`, `event_id`, `metadata`, and `waveform_id`.

- **Foreign-key constraints:**
  - `event_id` references `postmortem_events(id)` with a CASCADE on delete.

### 4. `postmortem_buffers`

Stores buffer data associated with postmortem events.

- **Columns:**
  - `buffer_id`: Unique identifier for the buffer. (Primary Key)
  - `event_id`: Reference to an event in `postmortem_events`.
  - `channel_id`: Identifier for the channel related to the buffer.
  - `postmortem_data`: Array of integers for the buffer data.
  - `sequence_id`: Integer for maintaining sequence information.

- **Indexes:**
  - Primary Key on `buffer_id`
  - BTree indexes on `channel_id` and `event_id`.

- **Foreign-key constraints:**
  - `event_id` references `postmortem_events(id)` with a CASCADE on delete.

- **Triggers:**
  - `trigger_update_sequence_id`: Before insert, executes function to update `sequence_id`.

## Entity-Relationship Diagram

For a visual representation of these relationships, consider generating an Entity-Relationship Diagram (ERD) based on the schema provided above. This can help visualize the connections between tables and the flow of data within your database.
